# Docker & Nuxt & Storybook Sample

## Nuxtの開発開始

```sh
$ docker-compose up nuxt
```

`http://localhost:3000` でローカルサーバーが起動します。

## Storybookの開発開始

```sh
$ docker-compose up storybook
```

`http://localhost:9999` でローカルサーバーが起動します。

## 同時に開発したいとき

```sh
$ docker-compose up nuxt storybook
```