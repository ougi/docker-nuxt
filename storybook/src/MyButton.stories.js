import Vue from "vue";
import MyButton from "nuxt-ssr-sample/components/MyButton.vue";

export default { title: "MyButton" };

export const Default = () => ({
  components: { MyButton },
  template: '<my-button title="Default3" />'
});

export const WithEmoji = () => ({
  components: { MyButton },
  template: '<my-button title="😀 😎 👍 💯" />'
});
